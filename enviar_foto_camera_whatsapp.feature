# language: pt
  Funcionalidade: Enviar uma foto da câmera do whatsapp
    Cenario: Enviar uma foto a partir da câmera da tela inicial
      Dado que eu esteja na tela inicial
      E selecione a câmera ao lado da aba Conversas
      Quando tirar a foto
      E escolher o contato
      E clicar no ícone de seta para direita
      Entao a foto será enviada para o contato selecionado

    Cenario: Enviar uma foto da câmera a partir de uma conversa existente
      Dado que eu esteja na tela de uma conversa
      Quando clicar no icone da câmera presente na área de digitação
      E tirar a foto
      E clicar no ícone de seta para direita
      Entao a imagem será enciada para a conversa aberta
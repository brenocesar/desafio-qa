# language: pt
  Funcionalidade: Realizar chamada para um contato
    Cenario: Realizar uma chamada de video para um contato
      Dado que eu esteja na aba Ligações
      Quando selecionar o ícone do telefone e um +
      E clicar no ícone de video de um contato
      Entao uma video chamada é realizada

    Cenario: Realizar uma chamada para um contato do histórico
      Dado que eu esteja na aba Ligações
      E possua um histórico de ligações
      Quando clicar no ícone do tipo de chamada realizada
      Entao uma chamada do mesmo tipo será realizada